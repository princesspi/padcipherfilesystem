<html>
<head>
<title>Browser Side Encryption System (BSES)</title>
<link rel="stylesheet" type="text/css" href="/Access/style.css">
</head>
<body>
<div id="content">
<?php include('menu.php'); ?>
<h1>Decrypt a File with PadCipher</h1>
Upload a <a href="encrypt.php">Message File</a>
<input id="message" name="message" type="file"><br>
Upload a <a href="generate.php">CypherKey</a>
<input name="pad" id="pad" type="file"><br><br>
<input type="button" onclick="encryptFile()" value="Decrypt">


<script src="downloader.js"></script>

<script>
function encryptFile() {
    var message = document.getElementById("message");
    var pad = document.getElementById("pad");

    var padreader = new FileReader();
    var messagereader = new FileReader();

    messagereader.onload = function(){
      var messagestr = messagereader.result;
      padreader.onload = function() { var padstr = padreader.result;
	  decrypt(messagestr,padstr); }
    };

    messagereader.readAsText(message.files[0]);
    padreader.readAsText(pad.files[0]);
}

function decrypt(messagestr,padstr) {
var encodedarr = new Array();
var out = new Array();
var decrypted = "";

    var padarr = padstr.split("-");
    var messagearr = messagestr.split("-"); 

    for(i=0; i<messagearr.length; i++) {
      out.push(parseInt(messagearr[i])-parseInt(padarr[i]));
    }

    for(i=0;i<out.length;i++) {
		decrypted += String.fromCharCode(out[i]);
    }

var file = 'd'+decrypted.slice(1);

download(file,"DecryptedFile.pad","text/plain");
}

</script>
</body>
</html>